import math
import numpy



# Trajektorienregelung als einfache Distanzregelung mit PID Regler

# Für Truck-Manöver: Lenkwinkel als Stellgröße
# Für Trailer-Manöver: Knickwinkel als Stellgröße --> Kaskadierung nötig

class Trajectory_Regulator:


	def __init__(self, k_p, k_i, k_d):
		# PID Regler Konstanten
		self.k_p = k_p
		self.k_i = k_i
		self.k_d = k_d

		# Zwischenspeicher für I-Anteil und D-Anteil des diskreten PID-Reglers
		self.offset_sum = 0.0
		self.offset_prev = 0.0
		self.orientation_sum = 0
		self.orientation_prev = 0.0
		self.orientation_prev = 0.0
		
		# Speichern der Stellgrößen zur späteren Auswertung
		self.steer_angle_data = []
		self.hitch_angle_data = []

		# Speichern der Regelabweichung zur späteren Auswertung
		self.offset_data = []
		
		

	# Berechnung der Distanz des Fahrzeuges (Bezugspunkt Hinterachse) zu gerader Trajektorie
	def calc_distance_to_parallel_route(self, rear_axis_posX, rear_axis_posY, location_vector, orientation_vector):

		diff_point_location = []
	
		diff_point_location.append(rear_axis_posX - location_vector[0])
		diff_point_location.append(rear_axis_posY - location_vector[1])


		# Kreuzprodukt von zwei 2 dimensionalen Vektoren ist ein Skalar
		# Kreuzprodult von der Differenz Ortsvektor Gerade und Punkt mit Normalenvektor 
		crossed_diff = diff_point_location[0] * orientation_vector[1] - diff_point_location[1] * orientation_vector[0]
	

		# In Ebenen-Normalform enstpricht der Normalenvektor dem Richtungsvektor der Geraden
		distance = crossed_diff / (math.sqrt(orientation_vector[0] ** 2 + orientation_vector[1] ** 2))

		return distance
	

	# Berechnung der Distanz des Fahrzeuges (Bezugspunkt Hinterachse) zu Kreis-Trajektorie
	def calc_distance_to_r_trajectory_center(self, pos_r_trajectory, rear_axis_posX, rear_axis_posY):
	
		distance = math.sqrt((rear_axis_posX - pos_r_trajectory[0]) ** 2 + (rear_axis_posY - pos_r_trajectory[1]) ** 2)

		return distance


	
		# Trajektorienregelung auf eine gerade Trajektorie mit PID-Regler, Stellgröße: Lenkwinkel
	def regulate_steering_straight(self, orientation_target, orientation,time_step):
		steer_angle_limit = 15
		# Regelabweichung:
		e = orientation_target - orientation
		
		# Integral der Regelabweichung		
		self.orientation_sum = self.orientation_sum  + e * time_step
		self.orientation_sum = max(-1, min(1, self.orientation_sum))

		# Berechnung der Stellgröße
		p = self.k_p * e
		i = self.k_i * self.orientation_sum
		d = self.k_d * (e - self.orientation_prev) / time_step
		steer_angle =  p + i + d		
		self.orientation_prev = e
		
		steer_angle = max(-steer_angle_limit, min(steer_angle, steer_angle_limit))
		print(e)
		print(steer_angle)

		return steer_angle
	

	# Trajektorienregelung auf eine Kreis-Trajektorie mit PID-Regler, Stellgröße: Lenkwinkel
	def regulate_steering_curved(self, pos_r_trajectory, rear_axis_posX, rear_axis_posY, steer_target, r_trajectory, time_step):

		# Distanz zur Trajektorie
		offset = self.calc_distance_to_r_trajectory_center(pos_r_trajectory, rear_axis_posX, rear_axis_posY) - r_trajectory

		# Integral der Regelabweichung
		self.offset_sum = self.offset_sum + offset

		# Berechnung der Stellgröße bzw. der nötigen Änderung der Stellgröße
		delta_steer_angle = self.k_p * offset + self.k_i * self.offset_sum * time_step + self.k_d * (offset - self.offset_prev) / time_step

		# Unterscheidung wo sich das Fahrzeug im Vergleich zur Kreistrajektorie befindet
		is_circle_first = (steer_target < 0)
		is_circle_second = (steer_target > 0)
		negative_offset = (offset < 0)
		positive_offset = (offset >= 0)

		# Angepasste Änderung des Lenkwinkels aufgrund der Positons des Fahrzeugs im Vergleich zur Kreistrajektorie
		if (is_circle_first and negative_offset):
			steer_angle = steer_target - delta_steer_angle
		if (is_circle_first and positive_offset):
			steer_angle = steer_target - delta_steer_angle

		if (is_circle_second and negative_offset):
			steer_angle = steer_target + delta_steer_angle
		if (is_circle_second and positive_offset):
			steer_angle = steer_target + delta_steer_angle
	
		self.offset_prev = offset

		self.steer_angle_data.append(steer_angle)
		self.offset_data.append(offset)

		return steer_angle

		
	# Trajektorienregelung auf eine gerade Trajektorie mit PID-Regler, Stellgröße: Knickwinkelwinkel
	def regulate_hitch_straight(self, trailer_posX, trailer_posY, location_vector, orientation_vector, time_step):

		# Distanz zur Trajektorie		
		offset = self.calc_distance_to_parallel_route(trailer_posX, trailer_posY, location_vector, orientation_vector)

		# Integral der Regelabweichung
		self.offset_sum = self.offset_sum + offset

		# Berechnung der Stellgröße
		hitch_angle = self.k_p * offset + self.k_i * self.offset_sum * time_step + self.k_d * (offset - self.offset_prev) / time_step
		
		self.offset_prev = offset

		self.hitch_angle_data.append(hitch_angle)
		self.offset_data.append(offset)

		return hitch_angle


	# Trajektorienregelung auf eine Kreis-Trajektorie mit PID-Regler, Stellgröße: Knickwinkel
	def regulate_hitch_curved(self, pos_r_trajectory, trailer_posX, trailer_posY, hitch_target, r_trajectory, time_step):

		# Distanz zur Trajektorie
		offset = self.calc_distance_to_r_trajectory_center(pos_r_trajectory, trailer_posX, trailer_posY) - r_trajectory
		
		# Integral der Regelabweichung
		self.offset_sum = self.offset_sum + offset


		# Berechnung der Stellgröße bzw. der nötigen Änderung der Stellgröße
		delta_hitch_angle = self.k_p * offset + self.k_i * self.offset_sum * time_step + self.k_d * (offset - self.offset_prev) / time_step


		# Unterscheidung wo sich das Fahrzeug im Vergleich zur Kreistrajektorie befindet
		is_circle_first = (hitch_target < 0)
		is_circle_second = (hitch_target > 0)
		negative_offset = (offset < 0)
		positive_offset = (offset >= 0)


		# Angepasste Berechnung des Lenkwinkels aufgrund der Positons des Fahrzeugs im Vergleich zur Kreistrajektorie
		if (is_circle_first and negative_offset):
			hitch_angle = hitch_target - delta_hitch_angle
		if (is_circle_first and positive_offset):
			hitch_angle = hitch_target - delta_hitch_angle

		if (is_circle_second and negative_offset):
			hitch_angle = hitch_target + delta_hitch_angle
		if (is_circle_second and positive_offset):
			hitch_angle = hitch_target + delta_hitch_angle
	
		self.offset_prev = offset

		self.hitch_angle_data.append(hitch_angle)
		self.offset_data.append(offset)

		return hitch_angle


# Trajektorienregelung als einfache Distanzregelung mit PID Regler und Geschwindigkeitsabhänigen Regelparametern
# Wird momentan nur beim Manöver mit Truck (Lenkwinkelregler) verwendet
class Trajectory_Regulator_Velocity_Dependent(Trajectory_Regulator):



	# Trajektorienregelung auf eine gerade Trajektorie mit PID-Regler, Stellgröße: Lenkwinkel
	def regulate_steering_straight(self, rear_axis_posX, rear_axis_posY, location_vector, orientation_vector, time_step, velocity, drive_mode):
		
		# Berechnung der geschwindigkeitsabhängigen Regelparameter
		self.straight_PID_params_for_velocity(velocity, drive_mode)

		# Distanz zur Trajektorie
		offset = self.calc_distance_to_parallel_route(rear_axis_posX, rear_axis_posY, location_vector, orientation_vector)
		
		# Integral der Regelabweichung		
		self.offset_sum = self.offset_sum + offset 

		# Berechnung der Stellgröße
		steer_angle = self.k_p * offset + self.k_i * self.offset_sum * time_step + self.k_d * (offset - self.offset_prev) / time_step
		
		self.offset_prev = offset

		self.steer_angle_data.append(steer_angle)
		self.offset_data.append(offset)

		return steer_angle
	

	# Trajektorienregelung auf eine Kreis-Trajektorie mit PID-Regler, Stellgröße: Lenkwinkel
	def regulate_steering_curved(self, pos_r_trajectory, rear_axis_posX, rear_axis_posY, steer_target, r_trajectory, time_step, velocity, drive_mode):

		# Berechnung der geschwindigkeitsabhängigen Regelparameter
		self.curved_PID_params_for_velocity(velocity, drive_mode)

		# Distanz zur Trajektorie
		offset = self.calc_distance_to_r_trajectory_center(pos_r_trajectory, rear_axis_posX, rear_axis_posY) - r_trajectory

		# Integral der Regelabweichung
		self.offset_sum = self.offset_sum + offset

		# Berechnung der Stellgröße bzw. der nötigen Änderung der Stellgröße
		delta_steer_angle = self.k_p * offset + self.k_i * self.offset_sum * time_step + self.k_d * (offset - self.offset_prev) / time_step

		# Unterscheidung wo sich das Fahrzeug im Vergleich zur Kreistrajektorie befindet
		is_circle_first = (steer_target < 0)
		is_circle_second = (steer_target > 0)
		negative_offset = (offset < 0)
		positive_offset = (offset >= 0)

		# Angepasste Berechnung des Lenkwinkels aufgrund der Positons des Fahrzeugs im Vergleich zur Kreistrajektorie
		if (is_circle_first and negative_offset):
			steer_angle = steer_target - delta_steer_angle
		if (is_circle_first and positive_offset):
			steer_angle = steer_target - delta_steer_angle

		if (is_circle_second and negative_offset):
			steer_angle = steer_target + delta_steer_angle
		if (is_circle_second and positive_offset):
			steer_angle = steer_target + delta_steer_angle
	
		self.offset_prev = offset

		self.steer_angle_data.append(steer_angle)
		self.offset_data.append(offset)

		return steer_angle

	# Geschwindigkeitsabhängige Kennlinie für PID-Parameter, ermittelt durch Versuche (Unterscheidung zwischen Gerader und Kurvenfahrt)
	# Geschwindigkeitsabhängige Kennlinie für PID-Parameter bei Fahrt auf gerader Trajektorie
	def straight_PID_params_for_velocity(self, velocity, drive_mode):

		velocity_kph = velocity * 3.6

		#Aufgrund quadratischer Approximation (Parabel) ...
		#Kennlinie gilt nur für Geschwindigkeiten zwischen 0 und 10 km/h, bei größeren Werten werden Parameter von 10 km/h verwendet
		if(velocity_kph > 10.0):
			velocity_kph = 10.0

		if(drive_mode == 1): #Forwards
			self.k_p = 2.81*(velocity_kph**2) - 52.44*velocity_kph + 294.34
			self.k_i = 0.20*(velocity_kph**2) - 4.03*velocity_kph + 30.50
			self.k_d = 8.30*(velocity_kph**2) - 144.56*velocity_kph + 691.96
			

		if(drive_mode == 3): #Backwards
			self.k_p = 7.05*(velocity_kph**2) - 97.25*velocity_kph + 410.29
			self.k_i = 1.34*(velocity_kph**2) - 17.49*velocity_kph + 70.69
			self.k_d = 43.65*(velocity_kph**2) - 520.9*velocity_kph + 1673.8
			


	# Geschwindigkeitsabhängige Kennlinie für PID-Parameter bei Fahrt auf Kurven-Trajektorie
	def curved_PID_params_for_velocity(self, velocity, drive_mode):
		
		velocity_kph = velocity * 3.6

		#Kurvenfahrt nur auf Rückwärtsfahrt zwischen 0 und 6.2 km/h (max Geschw. von TruckMaker) 
		# I-Anteil = 0, da Anti-Windup sonst auftritt und stationäre Genauigkeit ausreichend erreicht wird

		if(drive_mode == 3): #Backwards
			self.k_p = 12.5*(velocity_kph**2) - 177.5*velocity_kph + 717.5
			self.k_i = 0
			self.k_d = 27.5*(velocity_kph**2) - 364.5*velocity_kph + 1339
			





