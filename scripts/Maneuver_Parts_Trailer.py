import math
import time

from Ros_Handle_Topics import subs
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data
from Coordinate_system import Coordinate_System
from Parking_Spot_Detection import Parking_Space, detect_parking_space_side

# Klasse, welche die Teilmanöver für das seitliche Parken mit Anhänger beinhaltet
# Teilmanöver benötigen die Klasse der Subscriber (Zugriff auf Live-Daten des Simulationsprogramms zu kommen), die Klasse der Trajektorie (Zugriff auf Trajektorie-Parameter & -funktionen) und die Klasse des Fahrzeuges (Zugriff auf Fahrzeugparameter & -funktionen)

# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
velocity_forward = 50
velocity_backward = -40

class Maneuver_Parts_Trailer:

	# Konstrukor
	def __init__(self):
		# Werden Verwendet um innerhalb des Hauptdokuments die Teilmanöver durchzuwechseln
		self.state_park_prep = True
		self.state_roll_out = False
		self.state_roll_on = False
		self.state_circle_first = False
		self.state_switch_circle = False
		self.state_circle_second = False
		self.state_park_straighten = False
		self.state_park_finished = False

		self.velocity = velocity_forward
		
		self.start_ = time.time()



	# Teilmanöver für paralleles Vorbeifahren an der Parklücke
	def maneuver_park_prep(self, subscriber: subs, coords: Coordinate_System, vd: Vehicle_Data, td: Trajectory_Data, ps: Parking_Space, testing_without_lidar = False):
		print("Maneuver: Park Prep")

		# Setzen der Geschwindigkeit
		self.velocity = velocity_forward

		if testing_without_lidar == True:
			#### Für Test (kann ggf gelöscht werden) #################
			if time.time()-self.start_ > 0.5:
				self.state_park_prep = False
				self.state_roll_out = True

				# Reset Zeit
				td.time_driving_distance = time.time()

				# Trajektorie Stuff
				td.calculate_trajectory_data(vd)
				vd.save_hitch_angle(subscriber.get_hitch_angle())
			#######################################################

		else:
			# Parklückenerkennung
			detect_parking_space_side(subscriber, coords, ps)

			# Beenden des Manövers, sofern Parklücke erkannt wurde
			if ps.valide_parking_space == True and ps.side_parking_space == True:
				self.state_park_prep = False
				self.state_roll_out = True

				# Reset Zeit
				td.time_driving_distance = time.time()

				# Trajektorie
				# Setzen der Parklückenmaße für Trajektorie
				td.l_park = ps.length_parking_space		
				td.b_park = 1.25 * vd.b_truck
				td.d_parkinglot = ps.side_distance_to_vehicle
				# Berechnung der Trajektorie
				td.calculate_trajectory_data(vd)

				vd.save_hitch_angle(subscriber.get_hitch_angle())

		return self.velocity

	# Teilmanöver: Ausrollen der Vorwärtsfahrt und rückwärts anrollen
	def maneuver_park_roll(self, subscriber: subs, coords: Coordinate_System, vd: Vehicle_Data, td: Trajectory_Data):
		print("Maneuver: Park Roll")

		# Anhalten, Speichern der ausgerollten Distanz
		if self.state_roll_out == True:
			self.velocity = 0
			td.roll_out_distance += subscriber.get_velocity() * (time.time() - td.time_driving_distance)
			td.time_driving_distance = time.time()
			print("\tRoll out distance: ", td.roll_out_distance)
		
		# Fahrzeug ist angehalten angehalten
		if subscriber.get_velocity() < 1 and self.state_roll_on == False:
			self.velocity = velocity_backward
			self.state_roll_out = False
			self.state_roll_on = True
			self.driven_roll_on_distance = 0
			print("\tSwitch from roll out in roll on :)")
			# Speichern des anfänglichen Knickwinkels für die Knickwinkelegelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

			
		# Rückwärtsanfahren
		if self.state_roll_on == True:
			self.velocity = velocity_backward
			self.driven_roll_on_distance += (abs(subscriber.get_velocity()) * (time.time() - td.time_driving_distance))
			td.time_driving_distance = time.time()
			print("\tRoll on distance: ", self.driven_roll_on_distance)

		 

		# Einpark-Trajektorie beginnt nach Rückfahren der ausgerollten Distanz, dabei fährt das Gespann noch ein Stück gerade
		if self.state_roll_on == True and (td.roll_out_distance + (td.l_straight / 2)) < self.driven_roll_on_distance:
			
			self.velocity = velocity_backward
			
			self.driven_distance_trailer = 0
			self.driven_distance = 0
			td.time_driving_distance = time.time()

			self.state_roll_on = False
			self.state_circle_first = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

			# Berechnung der Kreismittelpunkte der Einparktrajektorie für Trajektorie-Regelung
			td.calc_r_trajectory_centers(coords.x_trailer, coords.y_trailer)

			# Berechnung der Switching Trajektorie
			td.calc_switching_trajectory()

			# Berechnung der Trajektorie der Parklücke
			td.calc_parking_lot_trajectory()

			print("\tDone")	
		return self.velocity


	# Teilmanöver: Erster Kreisbogen
	def maneuver_circle_first(self, subscriber: subs,  coords: Coordinate_System, vd: Vehicle_Data, td: Trajectory_Data):
		print("Maneuver: First Circle")

		# Rückwärtsfahren
		self.velocity = velocity_backward
	    
		print("\tDriven Distnace: ", self.driven_distance, "\tDriven Distnace Trailer: ", self.driven_distance_trailer, "\tLänge Kreisbogen 1: ", td.bgl_trajectory_park_1)
		# Beenden des Teilmanövers, wenn Ende des ersten Kreisbogens erreicht wurde
		if td.bgl_trajectory_park_1 < self.driven_distance:
			self.driven_distance = 0

			self.state_circle_first = False
			self.state_switch_circle = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		vel = subscriber.get_velocity()
		self.driven_distance_trailer += coords.calc_driven_distance_trailer(vel)
		self.driven_distance += (abs(vel) * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()
		
		return self.velocity


	# Teilmanöver: gerades Stück zwischen beiden Kreisbögen
	def maneuver_switch_circle(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		print("Maneuver: Switch Circle")

		# Rückwärtsfahren
		self.velocity = velocity_backward

		self.driven_distance += (abs(subscriber.get_velocity()) * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()

		print("\tDriven Distnace: ", self.driven_distance, "\tLänge Switch Circle: ", td.l_straight)
		# Beenden des Teilmanövers, wenn Ende des geraden Stücks erreicht wurde
		if (td.l_straight) < self.driven_distance:
			self.driven_distance = 0
			self.state_switch_circle = False
			self.state_circle_second = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		return self.velocity

	
	# Teilmanöver: Zweiter Kreisbogen
	def maneuver_circle_second(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		print("Maneuver: Second Circle")

		# Rückwärtsfahren
		self.velocity = velocity_backward

		self.driven_distance += (abs(subscriber.get_velocity()) * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()
		
		print("\tDriven Distnace: ", self.driven_distance, "\tLänge Kreisbogen 2: ", td.bgl_trajectory_park_2)
		# Beenden des Teilmanövers, wenn Ende des zweiten Kreisbogens erreicht wurde
		if td.bgl_trajectory_park_2 < self.driven_distance:
			self.driven_distance = 0
			self.state_circle_second = False
			self.state_park_straighten = True

			# Speichern des Hitch Winkels für die Hitch Winkel Regelung
			vd.save_hitch_angle(subscriber.get_hitch_angle())

		return self.velocity


	# Kurzes Teil-Manöver zum gerade Ziehen / zum Gespann "ent-knicken" innerhalb der Parklücke
	def maneuver_park_straighten(self, subscriber: subs, vd: Vehicle_Data, td: Trajectory_Data):
		print("Maneuver: Straighten")

		# Rückwärtsfahren
		self.velocity = velocity_backward

		self.driven_distance += (abs(subscriber.get_velocity()) * (time.time() - td.time_driving_distance))
		td.time_driving_distance = time.time()

		print("\tDriven Distnace: ", self.driven_distance, "\tLänge Straighten: ", (td.l_straight / 2))
		# Beenden des Parkens, wenn Endpunkt erreicht
		if ((td.l_straight / 2)) < self.driven_distance:
			self.state_park_straighten = False
			self.state_park_finished = True
			self.velocity = 0
		
		return self.velocity

