import time
import roslibpy
import math
import numpy


from Hitch_Angle_Reg import Reichenegger
from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Trailer
from Trajectory_Regulator import Trajectory_Regulator
from Data_Plot import plot_PID_straight, plot_hitch_regulator
from Data_Plot import plot_trajectory_trailer, plot_trajectory_truck_trailer
from Maneuver_Parts_Trailer import Maneuver_Parts_Trailer
from Coordinate_system import Coordinate_System
from Parking_Spot_Detection import Parking_Space


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz der Koordinaten System Klasse
myCoords = Coordinate_System(myVehicle)

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Trailer()

# Instanz Manöver
myMParts = Maneuver_Parts_Trailer()

# Instanz Parklücke
myParkingspace = Parking_Space(1)

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(30, 15, 0.25)
myReichenegger = Reichenegger()

# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
# --> Definition in Maneuver_Parts_Trailer
	
# ungefähre Zykluszeit mit dem die Loop durchgeführt wird in Sekunden --> In IPG Truckmaker 20 Hz eingestellt
time_step = 0.05


# Container zum Speichern von Lenk- und Knickwinkel

steer_target_straight = []
steer_actual = []
orientation_target = 0.0
orientation_actual = []

hitch_data_1 = []
steer_target_1 = []
steer_actual_1 = []
hitch_target_1 = 0.0

hitch_data_2 = []
steer_target_2 = []
steer_actual_2 = []
hitch_target_2 = 0.0

hitch_data_3 = []
steer_target_3 = []
steer_actual_3 = []
hitch_target_3 = 0.0

# Initiale Werte, die gepublished werden
pub_velocity = 0
steer_target = 0

steer_target = 0

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())
print("Coords for calc:", myCoords.x_hitch, myCoords.y_hitch, myCoords.x_rear, myCoords.y_rear)
myTrajectory.calc_trajectory_straight(myCoords.x_hitch, myCoords.y_hitch, myCoords.x_rear, myCoords.y_rear)
myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

time.sleep(5)
# Main Loop
while(not myMParts.state_park_finished):
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()

	# Alter Kommentar: Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	# Neu: Berechnung Positionen
	myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())


	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):

		# Regelung zum Fahren auf gerader Trajektorie
		orientation_target = 0
		steer_target = straight_PID.regulate_steering_straight(orientation_target, myCoords.truck_orientation, time_step)

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_park_prep(mySubs, myCoords, myVehicle, myTrajectory, myParkingspace, testing_without_lidar= False)

		# Speichern der Daten für Plots zur Auswertung
		steer_target_straight.append(steer_target)
		steer_actual.append(mySubs.get_steering_angle())
		orientation_actual.append(math.degrees(myCoords.truck_orientation))


	# Teilmanöver: Aus- und wieder Anrollen
	if (myMParts.state_roll_out == True or myMParts.state_roll_on == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_park_roll(mySubs, myCoords, myVehicle, myTrajectory)
		
		# Anhalten
		if (myMParts.state_roll_out == True):
		
			# Regelung zum Fahren auf gerader Trajektorie, nutzt Regler aus vorherigem Teilmanöver, da noch Vorwärts, gerade gefahren wird
			steer_target = straight_PID.regulate_steering_straight(orientation_target, myCoords.truck_orientation, time_step)


		# Wieder Anfahren, nun Rückwärts
		if (myMParts.state_roll_on == True):

			# Knickwinkel-Regler zum Einstellen des obig berechneten Knickwinkels
			hitch_target = 0
			steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())
		

		

	# Teilmanöver: erster Kreis zum Parken
	if (myMParts.state_circle_first == True):
		
		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_circle_first(mySubs, myCoords, myVehicle, myTrajectory)
		
		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = myVehicle.numeric_radius_to_hitch(myTrajectory.r_trajectory)
		print("\tRadius: ", myTrajectory.r_trajectory, "\tSoll-Hitch: ", hitch_target, "\tIst-Hitch: ", mySubs.get_hitch_angle())

		# Knickwinkel-Regler zum Einstellen des obig berechneten Knickwinkels
		steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())

		hitch_data_1.append(mySubs.get_hitch_angle())
		steer_target_1.append(steer_target)
		steer_actual_1.append(mySubs.get_steering_angle())
		hitch_target_1 = hitch_target


	# Teilmanöver: gerade Fahrt zwischen den beiden entgegengesetzten Kreisen, zum Wechseln des Knickwinkels
	if (myMParts.state_switch_circle == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_switch_circle(mySubs, myVehicle, myTrajectory)

		#hitch_angle = straight_PID.regulate_hitch_straight(myCoords.x_trailer, myCoords.y_trailer, myTrajectory.location_vector_switch_circle, myTrajectory.orientation_vector_switch_circle, time_step)
		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())


		hitch_data_2.append(mySubs.get_hitch_angle())
		steer_target_2.append(steer_target)
		steer_actual_2.append(mySubs.get_steering_angle())
		hitch_target_2 = hitch_angle
		

	# Teilmanöver: zweiter Kreis zum Parken
	if (myMParts.state_circle_second == True):

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_circle_second(mySubs, myVehicle, myTrajectory)

		# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
		hitch_target = - myVehicle.numeric_radius_to_hitch( myTrajectory.r_trajectory)

	
		# Regelung auf den gewünschten Knickwinkel
		steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())
		print("\tRadius: ", myTrajectory.r_trajectory, "\tSoll-Hitch: ", hitch_target, "\tIst-Hitch: ", mySubs.get_hitch_angle())

		hitch_data_3.append(mySubs.get_hitch_angle())
		steer_target_3.append(steer_target)
		steer_actual_3.append(mySubs.get_steering_angle())
		hitch_target_3 = hitch_target

		
	# Teilmanöver: Kurze gerade Fahrt, um Knickwinkel am Gespann in neutrale Position zu bringen
	if(myMParts.state_park_straighten == True):
		pub_velocity = myMParts.maneuver_park_straighten(mySubs, myVehicle, myTrajectory)
		
		#hitch_angle = straight_PID_end.regulate_hitch_straight(myCoords.x_trailer, myCoords.y_trailer, myTrajectory.location_vector_parking_lot, myTrajectory.orientation_vector_parking_lot, time_step)

		# Gespann soll sich wieder gerade ausrichten
		hitch_angle = 0.0

		# Regelung auf den gewünschten Knickwinkel
		steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_angle, hitch_angle - myVehicle.get_stored_hitch_angle())
	
	# Publishen der gewünschten Fahrbefehle
	print("\tIst-Lenkwinkel: ", mySubs.get_steering_angle(), "\tSoll-Lenkwinkel: ", steer_target)
	myPub.pub_velc_steer(pub_velocity, steer_target)

	# Speichern der Positionsdaten für spätere Auswertung und Analysis
	myVehicle.save_position_data(myCoords.x_trailer, myCoords.y_trailer)
	myVehicle.save_position_data_2(myCoords.x_rear, myCoords.y_rear)

# Anhalten des Trucks
myPub.pub_velc_steer(0, 0)

# Plot der Regler-Daten
plot_PID_straight(steer_target_straight, steer_actual, orientation_target, orientation_actual, time_step, straight_PID.k_p, straight_PID.k_i, straight_PID.k_d)
plot_hitch_regulator(steer_target_1, steer_actual_1, hitch_data_1, hitch_target_1, time_step, "First Circle")
plot_hitch_regulator(steer_target_2, steer_actual_2, hitch_data_2, hitch_target_2, time_step, "Switch Circle")
plot_hitch_regulator(steer_target_3, steer_actual_3, hitch_data_3, hitch_target_3, time_step, "Second Circle")

## Noch nicht nachgezogen
## Berechnen der Plot-Punkte der Trajektorie für die spätere grafische Darstellung
#myTrajectory.calc_straight_trajectory_data()
#myTrajectory.calc_parking_lot_trajectory_data()
#myTrajectory.calc_circle_first_data()
#myTrajectory.calc_switch_circle_data()
#myTrajectory.calc_circle_second_data()

## Plotten der Trajektorien zur Auswertung
#plot_trajectory_trailer(myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)

## Plotten der Trajektorien zur Auswertung
#plot_trajectory_truck_trailer(myVehicle.posX_data_2, myVehicle.posY_data_2, myVehicle.get_posX_data(), myVehicle.get_posY_data(), myTrajectory.trajectory_straight_data_posX, myTrajectory.trajectory_straight_data_posY, myTrajectory.trajectory_parking_lot_data_posX, myTrajectory.trajectory_parking_lot_data_posY, myTrajectory.circle_first_data_posX, myTrajectory.circle_first_data_posY, myTrajectory.switch_circle_data_posX, myTrajectory.switch_circle_data_posY, myTrajectory.circle_second_data_posX, myTrajectory.circle_second_data_posY)
	
