import roslibpy
import time
from truck_msgs.msg import zf_encoder


m_hitch_angle = 0
offset_hitch_angle = 0
m_steering = 0
offset_steering = 0


TRUCK_ID = 4

if TRUCK_ID == 4:
	full_hitch = 890 # 90°
	offset_hitch_angle = 1119 #1900 # 0°
	m_hitch_angle = - 90/(full_hitch-1900)
	full_steering = 660 # 30°
	offset_steering = 2160 # 0°
	m_steering = - 30/(full_steering-offset_steering)



# Client für Ros --> Rosbride mit WebSocket muss gelauncht werden --> roslaunch rosbridge_server rosbridge_websocket.launch
class client(object):
	client = roslibpy.Ros(host='localhost', port=9090)

	def start_client(self):
		self.client.run()
		print('Is ROS connected?', self.client.is_connected)

	def get_client(self):
		return self.client


# Klasse zum zwischen speichern der Subscribe Messages
class subs (object):

	current_velocity = 0.0		# Geschwindigkeit des Trucks
	current_rear_distance = 0.0	# Abstand nach Hinten zu detektierten Objekten
		
	current_hitch_angle = 0.0	# Winkel zwischen Truck und Trailer

	current_maneuver_time = 0.0 	# Manöver-Zeit

	current_side_distance  = 0.0	# Wert des Distanzsensors seitlich am Truck

	current_roll_angle = 0.0 	# Gesamte Strecke, welche die Räder zurück gelegt haben (seit Simulationsstart)

	current_posX = 0.0	# X-Positon der Vorderachse des Trucks
	current_posY = 0.0	# Y-Positon der Vorderachse des Trucks
	current_posX_trailer = 0.0	# X-Positon Achse Trailer des Trucks
	current_posY_trailer = 0.0	# Y-Positon Achse Trailer des Trucks

	current_steering_angle = 0.0	# Lenwinkel

	time_last_signal = 0.0		
	time_step = 0.05		# Zykluszeit

	callback_vel = False
	callback_side = False
	callback_hitch = False
	callback_steer = False



	# Abonnieren der Topics (einmal am Anfang des Codes)
	def subscribe_topics(self):
		self.sub_hitch_angle.subscribe(self.callback_hitch_angle)
		self.sub_velocity.subscribe(self.callback_velocity)
		self.sub_side_distance.subscribe(self.callback_side_distance)
		#self.sub_rear_distance.subscribe(self.callback_rear_distance)
		self.sub_steering_angle.subscribe(self.callback_steering_angle)
		



	# Definition und Instanzierung der Subscriber (einmal am Anfang des Codes)
	def init_subscription(self, client):
		# Definition der Topics die abonniert werden
		self.sub_hitch_angle =roslibpy.Topic(client, '/encoder', 'truck_msgs/zf_encoder')
		self.sub_velocity = roslibpy.Topic(client, '/speed', 'std_msgs/Int32')
		# self.sub_rear_distance = roslibpy.Topic(client, '/rear_distance', 'std_msgs/Float64')
		self.sub_side_distance = roslibpy.Topic(client, '/scan', 'sensor_msgs/LaserScan')
		self.sub_steering_angle = roslibpy.Topic(client, '/encoder', 'truck_msgs/zf_encoder')
		
		self.ros_client = client

		#ros_time_begin = self.ros_client.get_time().now()

		

	# Callbacks, welche die Werte in der abonnierten Topics in Klassenvariablen speichern

	
	def callback_velocity(self,msg):
		self.current_velocity = msg['data']
		self.callback_vel = True

	def callback_hitch_angle(self,msg): 
		self.current_hitch_angle = msg['trailer']
		self.callback_hitch = True


	def callback_side_distance(self,msg): 
		ranges = msg['ranges']
		self.current_side_distance = ranges[75]
		self.callback_side = True

	# def callback_rear_distance(self,msg): 
	# 	self.current_rear_distance = msg['data']
	# 	self.callback_counter += 1

	def callback_steering_angle(self,msg):
		self.current_steering_angle = msg['steering']
		self.callback_steer = True
		#print(" Time: [s]", format(((time.time()) % 1000), ".3f"), "Subs: ", self.current_steering_angle)
		#self.time_before = self.ros_client.get_time()


	# Einzelne Rückgaben der entsprechenden Subscribern

	def get_velocity(self):
		return self.current_velocity	

	def get_hitch_angle(self):
		# 0° --> 1461; 90° --> 2468
		# m = 45°C / (4046 - 3044)
		hitch_angle = m_hitch_angle *  (self.current_hitch_angle - offset_hitch_angle)
		return hitch_angle
	
	def get_side_distance(self):
		return self.current_side_distance

	# def get_rear_distance(self):
	#	return self.current_rear_distance

	def get_steering_angle(self):
		steering_angle = m_steering * (self.current_steering_angle - offset_steering)
		return steering_angle

	def get_time_step(self):
		return self.time_step

	# Warten zur Synchronisation mit Truckmaker
	def wait_for_signals(self):
		# Warteschleife: 11 Topics werden vom Truckmaker gepublisht, sobald alle eingetroffen sind, Freigabe für nächsten Simulationsschritt
		
		while not (self.callback_hitch and self.callback_steer and self.callback_vel and self.callback_side):
			pass
		self.callback_side = False
		self.callback_hitch = False
		self.callback_steer = False
		self.callback_vel = False

			


# Klasse zum publishen
class pub (object):


	# Definition und Instanzierung der Publisher (einmal am Anfang des Codes)
	def init_publition(self, client: client):
		# Definition der Topics die veröffentlich werden
		self.pub_velocity_control = roslibpy.Topic(client, '/control_speed', 'std_msgs/Int32')
		self.pub_steering_control = roslibpy.Topic(client, '/control_steer', 'std_msgs/Int32')

	
	# Publisher sollten jeden Simulationszyklus einmal aufgerufen werden, sonst kann es passieren, dass Truckmaker die Fahrbefehle überschreibt


	# Einzelner Publisher für Geschwindigkeit
	def pub_velc(self, vel_value):
		self.pub_velocity_control.publish(roslibpy.Message({'data':int(vel_value)}))

	# Einzelner Publisher für Lenkwinkel
	def pub_steer(self, steer_value):
		# steer_value in Degree --> scale for rostopic /control_steer
		steer_value_pub = steer_value * 10
		self.pub_steering_control.publish(roslibpy.Message({'data':int(steer_value_pub)}))


	# Gleichzeitiges Publishen von Lenkwinkel, Geschwindigkeit und Gang / Fahrmodus
	def pub_velc_steer(self, vel_value, steer_value):
		steer_value_pub = steer_value * 10
		self.pub_velocity_control.publish(roslibpy.Message({'data':int(vel_value)}))	
		self.pub_steering_control.publish(roslibpy.Message({'data':int(steer_value_pub)}))
		
