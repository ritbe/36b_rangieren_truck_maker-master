from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Coordinate_system import Coordinate_System
from Hitch_Angle_Reg import Reichenegger
from Data_Plot import plot_hitch_regulator
import time


# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz der Koordinaten System Klasse
myCoords = Coordinate_System(myVehicle)

# Instanz des Reglers um auf Trajektorie zu Fahren
myReichenegger = Reichenegger()

# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
# --> Definition in Maneuver_Parts_Trailer
	
# ungefähre Zykluszeit mit dem die Loop durchgeführt wird in Sekunden --> In IPG Truckmaker 20 Hz eingestellt
time_step = 0.05


# Initiale Werte, die gepublished werden
pub_velocity = 0
pub_steering = 0

steer_target = 0


hitch_data_1 = []
steer_target_1 = []
steer_actual_1 = []
hitch_target_1 = 0.0

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

radius = 1000

# Main Loop
start_ = time.time()
while(time.time() - start_ < 80):
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()


	# Berechnung Positionen
	myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())
	pub_velocity = -50
	# Berechnung des Knickwinkels, welcher für die Kreisfahrt benötigt wird
	hitch_target = - myVehicle.numeric_radius_to_hitch(radius)

	print("\tRadius: ", radius, "\tSoll-Hitch: ", hitch_target, "\tIst-Hitch: ", mySubs.get_hitch_angle())
	
    # Knickwinkel-Regler zum Einstellen des obig berechneten Knickwinkels
	steer_target = myReichenegger.calc_steer_angle_reichenegger(mySubs.get_velocity(), myVehicle.l_hitch, myVehicle.l_truck, myVehicle.l_trailer, mySubs.get_hitch_angle(), hitch_target, hitch_target - myVehicle.get_stored_hitch_angle())

	hitch_data_1.append(mySubs.get_hitch_angle())
	steer_target_1.append(steer_target)
	steer_actual_1.append(mySubs.get_steering_angle())
	hitch_target_1 = hitch_target
	
	# Publishen der gewünschten Fahrbefehle
	print("\tIst-Lenkwinkel: ", mySubs.get_steering_angle(), "\tSoll-Lenkwinkel: ", -steer_target)
	myPub.pub_velc_steer(pub_velocity, steer_target)
	
myPub.pub_velc_steer(0, 0)	
plot_hitch_regulator(steer_target_1, steer_actual_1, hitch_data_1, hitch_target_1, time_step, "Circle")

