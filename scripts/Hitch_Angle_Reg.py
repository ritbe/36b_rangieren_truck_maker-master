import math

class Reichenegger():
	def __init__(self):
		self.deviation_integral = 0

	# Funktion zur Berechnung des Lenkwinkels
	def calc_steer_angle_reichenegger(self, current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle, hitch_angle_target, delta_hitch_angle_start):
		current_velocity = current_velocity * 2
		
		# Umrechnung in rad
		current_hitch_angle_rad = math.radians(current_hitch_angle)
		hitch_angle_target_rad = math.radians(hitch_angle_target)

		# Polynom zur Berechnung der Regelparameter in Abhängigkeit des Zielknickwinkels
		if abs(hitch_angle_target_rad) >= 0.05:
			lambda_1 = 1.90 * 10**(-5) * (abs(hitch_angle_target_rad)**2) - 3.76 * 10**(-4) * abs(hitch_angle_target_rad) - 0.4285  # Polynomial of second degree
		# Exception for small hitch angles: Not included in the polynomial
		else:
			lambda_1 = -0.0859

		lambda_2 = -6.76 * 10**(-7) * (abs(hitch_angle_target_rad)**4) + 8.53 * 10**(-5) * (abs(hitch_angle_target_rad)**3) - 3.23 * 10**(-3) * (abs(hitch_angle_target_rad)**2) + 0.0157 * abs(hitch_angle_target_rad) + 2.3  # Polynomial of fourth degree


		delta_hitch_angle_rad = hitch_angle_target_rad - current_hitch_angle_rad

		g = g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle_rad)
		f = f_phi(current_velocity, l_trailer, current_hitch_angle_rad, hitch_angle_target_rad)

		# Verhindert das bei u durch 0 geteilt wird
		if g == 0:
			g = 0.00001

		u = (1 / g) * (-f - lambda_1 * hitch_angle_target_rad  - lambda_2 * delta_hitch_angle_rad)

		steering_angle = math.degrees(math.atan(u))
		
		# Begrenzung des maximalen Lenkwinkels zur naturgetreuen Darstellung --> 25° wird als Wert für das Projekt angenommen
		if steering_angle > 30:
			steering_angle = 30
		if steering_angle < -30:
			steering_angle = -30

		return steering_angle

# Mathematische Teilfunktion für die Lenkwinkelberechnung
def g_phi(current_velocity, l_hitch, l_truck, l_trailer, current_hitch_angle_rad):
	g = (current_velocity / l_truck) - (current_velocity * l_hitch * math.cos(current_hitch_angle_rad)) / (l_truck * l_trailer)
	return g

# Mathematische Teilfunktion für die Lenkwinkelberechnung
def f_phi(current_velocity, l_trailer, current_hitch_angle_rad, hitch_angle_target_rad):
	f = hitch_angle_target_rad + (current_velocity * math.sin(current_hitch_angle_rad)) / l_trailer
	return f


class PIDRegler:
    def __init__(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.previous_error = 0
        self.integral = 0
	
        self.max_lenkwinkel = 30

    def regulate(self, soll_knickwinkel, aktueller_knickwinkel):
        fehler = soll_knickwinkel - aktueller_knickwinkel

        # Proportionaler Anteil
        p = self.kp * fehler

        # Integraler Anteil
        self.integral += fehler
        i = self.ki * self.integral

        # Differenzieller Anteil
        derivative = fehler - self.previous_error
        d = self.kd * derivative

        # Berechnung des Lenkwinkels
        lenkwinkel = p + i + d

        # Aktualisierung des vorherigen Fehlers
        self.previous_error = fehler
	
        lenkwinkel = max(min(lenkwinkel, self.max_lenkwinkel), -self.max_lenkwinkel)

        return lenkwinkel

