import math
import time

from Ros_Handle_Topics import subs
from Coordinate_system import Coordinate_System



#Klasse: Parklücke
class Parking_Space:

    def __init__(self, parking_space_number):
        self.parking_space_number = parking_space_number    #Parklückennummer
        self.side_parking_space = False                     #Parklücke Seitwärts 
        self.reverse_parking_space = False                  #Parklücke Rückwärts
        self.start_detected = False                         #Zustandsvariable Parklückenbeginn erkannt
        self.end_detected = False                           #Zustandsvarianle Parklückenende erkannt
        self.valide_parking_space = False                   #Zustandsvariable valide Parklücke
        self.start_time_measurement = 0.0                   #Startzeit Parklücke erkannt
        self.stop_time_measurement = 0.0                    #Stopzeit Parklücke beendet
        self.x_trailer_start = 0                            #Koordinatenposition x bei Start von Parklücke
        self.y_trailer_start = 0                            #Koordinatenposition y bei Start von Parklücke
        self.x_trailer_stop = 0                             #Koordinatenposition x bei Ende von Parklücke
        self.y_trailer_stop = 0                             #Koordinatenposition y bei Ende von Parklücke
        self.side_distance_1 = 0
        self.side_distance_2 = 0
        self.side_distance_to_vehicle = 0                   #seitlicher Abstand zum nächsten Auto
        self.length_parking_space = 0                       #Länge der Parklücke
        self.distance_side = 530 / 1000                     #Mindestbreite seitliches Einparken (Umrechnung auf Meter, da Laser Daten in Meter liefert)
        self.distance_reverse = 2500 / 1000                 #Mindestbreite rückwärts Einparken (Umrechnung auf Meter, da Laser Daten in Meter liefert)
        self.length_side = 3150                             #Mindestlänge seitliches Einparken
        self.length_reverse = 400                           #Mindestlänge rückwärts Einparken
                
        self.side_distances = []
        self.times = []
        self.speed = []
        self.current_length = 0
        self.start = time.time()

             
   
#Funktion: Erkennung und Vermessung der seitlichen Parklücke   
def detect_parking_space_side(subscriber: subs, coords: Coordinate_System, ps: Parking_Space):
    print("\tErfassung Parklücke")
    ps.side_distances.append(subscriber.get_side_distance())
    ps.speed.append(subscriber.get_velocity)
    ps.times.append(time.time() - ps.start)

    #Erfassung des Beginns der Parklücke
    if((subscriber.get_side_distance() > ps.distance_side) and (ps.start_detected == False)):

        ps.start_time_measurement = time.time()
        ps.x_trailer_start = coords.x_trailer
        ps.y_trailer_start = coords.y_trailer
        ps.start_detected = True
        print("Start detection")
        test_detect_parking_space(ps)

    else:
        ps.side_distance_1 = subscriber.get_side_distance()


    if subscriber.get_side_distance() > 0.01:
        if((subscriber.get_side_distance() > ps.distance_side) and (ps.start_detected == True)):
            i = len(ps.speed) - 1
            delta_t = ps.times[i] - ps.times[i-1]
            avg_speed = (ps.speed[i]() + ps.speed[i-1]()) / 2.0
            print("Time" + str(ps.times[i]))
            print("Time difference" + str(delta_t))
            print("Average speed" + str(avg_speed))
            print("Added length " + str(avg_speed * delta_t))
            ps.current_length += avg_speed * delta_t

        if((subscriber.get_side_distance() < ps.distance_side) and ps.start_detected):
            ps.stop_time_measurement = time.time()
            ps.x_trailer_stop = coords.x_trailer
            ps.y_trailer_stop = coords.y_trailer
            ps.end_detected  = True
            test_detect_parking_space(ps)
                            
            #Berechnung der Parklückenmaße und Validierung der Parklücke
            if(ps.current_length > ps.length_side):
                            
                ps.valide_parking_space = True
                ps.side_parking_space = True
                print("Calculated (end_time-start_time)*speed_at_end" + str(subscriber.get_velocity()*(ps.stop_time_measurement - ps.start_time_measurement)))
                ps.length_parking_space = ps.current_length
                constant_distance_counter(subscriber, 0.0001)
                ps.side_distance_2 = subscriber.get_side_distance()
                if ps.side_distance_1 < ps.side_distance_2:
                    ps.side_distance_to_vehicle = ps.side_distance_1
                else:
                    ps.side_distance_to_vehicle = ps.side_distance_2
                test_detect_parking_space(ps)

            else:
                ps.start_detected = False
                ps.end_detected = False
                print("Parking Lot: Length to small!")

# def detect_parking_space_side(subscriber: subs, coords: Coordinate_System, ps: Parking_Space):
#     print("\tErfassung Parklücke")
#     ps.side_distances.append(subscriber.get_side_distance())
#     ps.times.append(time.time() - ps.start)


#     path = 0.0  # Initialize the path as 0
#     n = len(speed)

#     # Apply the trapezoidal rule to integrate the speeds
#     for i in range(1, n):
#         delta_t = time[i] - time[i-1]
#         avg_speed = (speeds[i] + speeds[i-1]) / 2.0
#         path += avg_speed * delta_t

#     #Erfassung des Beginns der Parklücke
#     if((subscriber.get_side_distance() > ps.distance_side) and (ps.start_detected == False)):

#         ps.start_time_measurement = time.time()
#         ps.x_trailer_start = coords.x_trailer
#         ps.y_trailer_start = coords.y_trailer
#         ps.start_detected = True
#         test_detect_parking_space(ps)

#     else:
#         ps.side_distance_1 = subscriber.get_side_distance()
              
#     #Erfassung des Endes der Parklücke
#     if subscriber.get_side_distance() > 0.01:
#         if((subscriber.get_side_distance() < ps.distance_side) and ps.start_detected):
#             ps.stop_time_measurement = time.time()
#             ps.x_trailer_stop = coords.x_trailer
#             ps.y_trailer_stop = coords.y_trailer
#             ps.end_detected  = True
#             test_detect_parking_space(ps)
                            
#             #Berechnung der Parklückenmaße und Validierung der Parklücke
#             if((subscriber.get_velocity()*(ps.stop_time_measurement - ps.start_time_measurement)) > ps.length_side):
                            
#                 ps.valide_parking_space = True
#                 ps.side_parking_space = True
#                 ps.length_parking_space = subscriber.get_velocity()*(ps.stop_time_measurement - ps.start_time_measurement)
#                 constant_distance_counter(subscriber, 0.0001)
#                 ps.side_distance_2 = subscriber.get_side_distance()
#                 if ps.side_distance_1 < ps.side_distance_2:
#                     ps.side_distance_to_vehicle = ps.side_distance_1
#                 else:
#                     ps.side_distance_to_vehicle = ps.side_distance_2
#                 test_detect_parking_space(ps)

#             else:
#                 ps.start_detected = False
#                 ps.end_detected = False
#                 print("Parking Lot: Length to small!")

#Funktion: Testfunktion von detect_parking_space_side und detect_parking_space_reverse
def test_detect_parking_space(ps:Parking_Space):

    if(ps.start_detected == True and ps.end_detected == False):
        print("Start X-Position Trailer:" + str(ps.x_trailer_start))
        print("Start Y-Position Trailer:" + str(ps.y_trailer_start))
    
    if(ps.start_detected == True and ps.end_detected == True and ps.valide_parking_space == False):
        print("Stop X-Position Trailer:" + str(ps.x_trailer_stop))
        print("Stop Y-Position Trailer:" + str(ps.y_trailer_stop))
    
    #Vergleich Parklückenlänge über Rechnung mit der Zeit und Parklückenlänge über Rechnung mit Koordinaten
    if(ps.start_detected == True and ps.end_detected == True and ps.valide_parking_space == True):
        print("Length of parking space from class Parking_Space:" + str(ps.length_parking_space))
        calculated_length = math.sqrt(math.pow((ps.x_trailer_stop-ps.x_trailer_start),2)+math.pow((ps.y_trailer_stop-ps.y_trailer_start),2))
        print("Length of parking space calculated" + str(calculated_length))
        if(calculated_length > ps.length_side):
            print("Valide parking space: TRUE")
        else:
            print("Valide parking space: FALSE")
        print("Side distance:" + str(ps.side_distance_to_vehicle))


#Funktion: Zur Erkennung der seitlichen Wand des Endes der Parklücke
def constant_distance_counter(subscriber: subs, distance_threshold):
    count = 0  # Variable zum Zählen der konstanten Abstände
    first_distance = None  # Variable zur Speicherung des ersten Abstands
    
    while count < 125:
        distance = subscriber.get_side_distance()
        
        if first_distance is None:
            first_distance = subscriber.get_side_distance()  # Speichere den ersten Abstand
        
        if abs(subscriber.get_side_distance() - first_distance) <= distance_threshold:
            count += 1  # Wenn der Abstand im Schwellenwert liegt, erhöhe den Zähler
            print("Konstanter Abstand gefunden! Zählerstand:", count)
        else:
            count = 0
            first_distance = None  # Setze den Zähler zurück, wenn der Abstand nicht konstant ist
        
      # Setze den ersten Abstand zurück, um beim nächsten Aufruf neu zu beginnen


# def constant_distance_counter(subscriber: subs, distance_threshold):
#     count = 0  # Variable zum Zählen der konstanten Abstände
#     previous_distance = None  # Variable zur Speicherung des vorherigen Abstands
    
#     while count < 20:
        
#         distance = subscriber.get_side_distance()
        
#         if previous_distance is not None and abs(distance - previous_distance) <= distance_threshold:
#             count += 1  # Wenn der Abstand im Schwellenwert liegt, erhöhe den Zähler
#             print("Konstanter Abstand gefunden! Zählerstand:", count)
#         else:
#             count = 0  # Setze den Zähler zurück, wenn der Abstand nicht konstant ist
        
    
#         previous_distance = distance  # Aktualisiere den vorherigen Abstand für den nächsten Schleifendurchlauf
      
# def constant_distance_counter(subscriber: subs, distance_threshold):
#     count = 0  # Variable zum Zählen der konstanten Abstände
#     dist_counter = 0
#     first_distance = 0  # Variable zur Speicherung des vorherigen Abstands
    
#     if(count == 0):

#         first_distance = subscriber.get_side_distance()
#         if previous_distance is not None and abs(distance - previous_distance) <= distance_threshold:
#             dist_counter +=1


#     while count < 20:
        
#         distance = subscriber.get_side_distance()
        
#         if previous_distance is not None and abs(distance - previous_distance) <= distance_threshold:
#             count += 1  # Wenn der Abstand im Schwellenwert liegt, erhöhe den Zähler
#             print("Konstanter Abstand gefunden! Zählerstand:", count)
#         else:
#             count = 0  # Setze den Zähler zurück, wenn der Abstand nicht konstant ist
        
    
#         previous_distance = distance  # Aktualisiere den vorherigen Abstand für den nächsten Schleifendurchlauf



#Funktion: Erkennung und Vermessung der Parklücke Rückwärts Einparken
def detect_parking_space_reverse(subscriber: subs, coords: Coordinate_System, ps: Parking_Space):

    #Erfassung des Beginns der Parklücke
    if((subscriber.get_side_distance() > ps.distance_reverse) and (ps.start_detected == False)):

        ps.start_time_measurement = time.time()
        ps.x_trailer_start = coords.x_trailer
        ps.y_trailer_start = coords.y_trailer
        ps.start_detected = True

    #Erfassung des Endes der Parklücke          
    if((subscriber.get_side_distance() < ps.distance_reverse) and ps.start_detected):
        ps.stop_time_measurement = time.time()
        ps.x_trailer_stop = coords.x_trailer
        ps.y_trailer_stop = coords.y_trailer
        ps.end_detected  = True
                         
        #Berechnung der Parklückenmaße und Validierung der Parklücke
        if((subscriber.get_velocity()*(ps.stop_time_measurement - ps.start_time_measurement)) > ps.length_reverse):
                        
            ps.valide_parking_space = True
            ps.side_parking_space = True
            ps.length_parking_space = subscriber.get_velocity()*(ps.stop_time_measurement - ps.start_time_measurement)
            ps.side_distance_to_vehicle = subscriber.get_side_distance()

        else:
            ps.start_detected = False
            ps.end_detected = False
            
               
         
       
