from Ros_Handle_Topics import subs, client
from Coordinate_system import Coordinate_System
from Vehicle_Parameter import Vehicle_Data
import time
import matplotlib.pyplot as plt

test_steering = False

myVehicle = Vehicle_Data()
myClient = client()
mySubs = subs()

# Starten des Ros-Clients
myClient.start_client()
# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

if test_steering:
    while True:
         steer = mySubs.get_steering_angle()
         print("Steering [°]: " + str(steer))

myCoords = Coordinate_System(myVehicle)

x = []
y = []

x_trailer = []
y_trailer = []

distances = []
times = []


driven_distance = 0
driven_distance_trailer = 0

start = time.time()
first_iteration = True

while time.time() - start < 40:
    time.sleep(0.05)

    vel = mySubs.get_velocity()
    steer = mySubs.get_steering_angle()
    hitch = mySubs.get_hitch_angle()
    print(str(vel) + "\t" + str(steer) + "\t" + str(hitch))
    myCoords.calc_positions(vel, steer, hitch)
    x.append(myCoords.x)
    y.append(myCoords.y)
    x_trailer.append(myCoords.x_trailer)
    y_trailer.append(myCoords.y_trailer)

    if not first_iteration:
        driven_distance += myCoords.driven_distance
        driven_distance_trailer += myCoords.calc_driven_distance_trailer(vel)
        # print(driven_distance, "\t", driven_distance_trailer)
    first_iteration = False

plt.figure(figsize=(10, 10))
plt.plot(x, y, label = "Truck")
plt.plot(x_trailer, y_trailer, label = "Trailer")
plt.legend()
plt.axis('equal')
plt.xlabel("x [mm]")
plt.ylabel("y [mm]")
#plt.xlim(-500,500)
#plt.ylim(-500,500)
plt.title("Truck Route x-y")
plt.show()
