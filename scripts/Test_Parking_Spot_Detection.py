from Ros_Handle_Topics import client
from Ros_Handle_Topics import subs
from Ros_Handle_Topics import pub
from Vehicle_Parameter import Vehicle_Data
from Trajectory_Parameter import Trajectory_Data_Trailer
from Trajectory_Regulator import Trajectory_Regulator
from Maneuver_Parts_Trailer import Maneuver_Parts_Trailer
from Coordinate_system import Coordinate_System
from Parking_Spot_Detection import Parking_Space

import matplotlib.pyplot as plt

# Instanz der Ros Klassen
myClient = client()
mySubs = subs()
myPub = pub()

# Instanz Fahrzeug Klassen
myVehicle = Vehicle_Data()

# Instanz der Koordinaten System Klasse
myCoords = Coordinate_System(myVehicle)

# Instanz Trajektorie Berechnung
myTrajectory = Trajectory_Data_Trailer()

# Instanz Manöver
myMParts = Maneuver_Parts_Trailer()

# Instanz Parklücke
myParkingspace = Parking_Space(1)

# Instanz des Reglers um auf Trajektorie zu Fahren
straight_PID = Trajectory_Regulator(20, 10, 0.1)


# Geschwindigkeit mit dem das Manöver durchgeführt werden soll
# --> Definition in Maneuver_Parts_Trailer
	
# ungefähre Zykluszeit mit dem die Loop durchgeführt wird in Sekunden --> In IPG Truckmaker 20 Hz eingestellt
time_step = 0.05


# Initiale Werte, die gepublished werden
pub_velocity = 0
pub_steering = 0

steer_target = 0

# Starten des Ros-Clients
myClient.start_client()

# Initialisiere Topics	
mySubs.init_subscription(myClient.get_client())
myPub.init_publition(myClient.get_client())

# Topics abonnieren
mySubs.subscribe_topics()

# Warten bis Ros-Kommunikation vollständig da ist
mySubs.wait_for_signals()

# Vektor berechnen für die parallele Trajektorie zum Ausmessen
myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())
print("Coords for calc:", myCoords.x_hitch, myCoords.y_hitch, myCoords.x_rear, myCoords.y_rear)
myTrajectory.calc_trajectory_straight2(myCoords.x, myCoords.y, myCoords.x_rear, myCoords.y_rear)
myVehicle.save_hitch_angle(mySubs.get_hitch_angle())

# Main Loop
while(myMParts.state_park_prep == True):
	# Warten, bis ROS neuer Simulationstakt sendet
	mySubs.wait_for_signals()

	# Alter Kommentar: Bestimmung Trailer Hinterachse Position, nötig für die Regelung der Trajektorie beim Rückwärtsfahren
	# Neu: Berechnung Positionen
	myCoords.calc_positions(mySubs.get_velocity(), mySubs.get_steering_angle(), mySubs.get_hitch_angle())


	# Teilmanöver: Vorbeifahren an Parklücke
	if (myMParts.state_park_prep == True):
		

		# Regelung zum Fahren auf gerader Trajektorie
		orientation_target = 0
		steer_target = straight_PID.regulate_steering_straight(orientation_target, myCoords.truck_orientation, time_step)

		# Lenkwinkelvorgabe inklusive begrenzter Lenkwinkelgeschwindigkeit
		pub_steering = mySubs.get_steering_angle() + myVehicle.slowdown_steering_angle(mySubs.get_time_step(), steer_target, mySubs.get_steering_angle())

		# Eigentliches Ausführen des Teilmanövers, welches auch den Fahrmodus angibt (Vorwärts, Rückwärts oder Anhalten)
		pub_velocity = myMParts.maneuver_park_prep(mySubs, myCoords, myVehicle, myTrajectory, myParkingspace, testing_without_lidar = False)


	
	# Publishen der gewünschten Fahrbefehle
	print("\tSide Distance: ", mySubs.get_side_distance())
	side_distances_ = []
	side_distances_.append(mySubs.get_side_distance())
	print(side_distances_)
	print("\tIst-Lenkwinkel: ", mySubs.get_steering_angle(), "\tSoll-Lenkwinkel: ", -steer_target)
	print("\tIst-Speed: ", mySubs.get_velocity(), "\tSoll-Speed: ", pub_velocity)
	myPub.pub_velc_steer(pub_velocity, steer_target)
	
print("Testing Done")

pub_velocity, steer_target = 0, 0
myPub.pub_velc_steer(pub_velocity, -steer_target)

plt.plot(myParkingspace.times, myParkingspace.side_distances)
plt.xlabel("Time [s]")
plt.ylabel("Side distance [m]")
plt.show()


